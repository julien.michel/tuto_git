#!/usr/bin/env python

"""Joystick teleoperation node.

This node computes velocity commands from joystick messages.

It subscribes to:
    - `/joy` (sensor_msgs/Joy): joystick message.

It publishes to :
    - `/twist_mux/input/teleop_joy` (geometry_msgs/Twist):
        command velocity.
"""
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

class TeleopJoy:
    """Joystick teleoperation."""

    def __init__(self):
        """Create publisher and subscriber."""
        # publisher
        self.cmd_vel_pub = rospy.Publisher('/twist_mux/input/teleop_joy',
                                           Twist, queue_size=1)
        # subscriber
        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cb)
    
    def joy_cb(self, msg):
        """Compute and publish twist command."""
        # initialize message
        cmd_vel_msg = Twist()
        # fill message
        cmd_vel_msg.linear.x = 0.22*msg.axes[1]
        cmd_vel_msg.linear.y = 0.0
        cmd_vel_msg.linear.z = 0.0
        cmd_vel_msg.angular.x = 0.0
        cmd_vel_msg.angular.y = 0.0
        cmd_vel_msg.angular.z = 2.84*msg.axes[0]
        # publish message
        self.cmd_vel_pub.publish(cmd_vel_msg)

def main():
    """Instantiate node and class."""
    # declare node
    rospy.init_node('teleop_joy')
    # instantiate class
    teleop_joy = TeleopJoy()
    # run
    rospy.spin()

if __name__ == '__main__':
    main()