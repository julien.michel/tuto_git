#!/bin/bash

#1.2
source /opt/ros/noetic/setup.bash
echo -e "\n# ROS setup\nsource /opt/ros/noetic/setup.bash" >> ~/.bashrc

#1.3
mkdir -p ~/catkin_ws/src
catkin init -w ~/catkin_ws
catkin build -w ~/catkin_ws
source ~/catkin_ws/devel/setup.bash
echo -e "\n# ROS catkin workspace\nsource ~/catkin_ws/devel/setup.bash" >> ~/.bashrc

#1.4
wget https://colas.gitlabpages.inria.fr/st5_robotique_autonome/_downloads/bbddbab82d3320e054a73c0ad466d2e1/st5_generic.tgz
tar -xvzf st5_generic.tgz -C ~/catkin_ws/src/
catkin build -w ~/catkin_ws
source ~/catkin_ws/devel/setup.bash
rosrun st5_generic test_install.py
